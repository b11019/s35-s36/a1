// required modules
const express = require('express');
const mongoose = require('mongoose');

const port = 4000;
const app = express();

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.hpwzu.mongodb.net/tasks182?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection
db.on('error',console.error.bind(console, "DB Connection Error"));

db.once('open', () => console.log('Successfully connected to MongoDB'));
app.use(express.json());

// reading of data forms
app.use(express.urlencoded({extended: true}));

const taskRoutes = require ('./routes/taskRoutes');
app.use('/tasks', taskRoutes);

const userRoutes = require ('./routes/userRoutes');
app.use('/users', userRoutes);



app.listen(port, ()=> console.log(`Server is running at port ${port}`));







// Activity 2:

//     >> Create 2 new routes and controllers in our userRoutes and userControllers.

//     >> Create a route and controller to update a single user's username field to our input from a request body.
//         -endpoint: '/:id'
//         -Create a new controller which is able to get the id from the url through the use of req.params:
//             -Create an updates object and add the new value from our req.body as the new value to the username field.

//             -Add a findByIdAndUpdate method from the User model and pass the id from req.params as its first argument.

//             -Add the updates object as its second argument.

//             -Add {new:true} as its third argument so the result would return the updated document instead of the old one.

//             -Then pass the result to the client.

//             -Catch the error and pass the error to the client.

        
//     >> Create a route and controller to get a single user.
//             -endpoint: '/getSingleUser/:id'
            
//             -Create a new controller which is able to get the id of the user from the url through the use of req.params.
//                 -In the controller, add a findById method from the User model and pass the id from req.params as its argument.
//                 -Then, send the result to the client.
//                 -Catch the error and send the error to the client.