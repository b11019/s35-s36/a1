const express = require('express');
const router = express.Router();
const taskControllers = require('../controllers/taskControllers');

// create task route
console.log(taskControllers);

router.post('/', taskControllers.createTaskController)
router.get('/', taskControllers.getAllTasksController)

router.get('/getSingleTask/:id', taskControllers.getSingleTaskController);


router.put('/updateTask/:id',taskControllers.updateTaskStatusController);
module.exports = router;