const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers');

// create task route

router.post('/', userControllers.createUserController)
router.get('/', userControllers.getAllUsersController)
router.put('/:id',userControllers.updateUserStatusController)
router.get('/getSingleUser/:id', userControllers.getSingleUserController);
module.exports = router; 